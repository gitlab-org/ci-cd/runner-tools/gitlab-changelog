package gitlab_test

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/gitlab"
)

func TestClientNotCreatedError_Error(t *testing.T) {
	e := gitlab.NewClientNotCreatedError(assert.AnError)
	assert.Contains(t, e.Error(), "couldn't create base GitLab client")
	assert.Contains(t, e.Error(), assert.AnError.Error())
}

func TestClientNotCreatedError_Unwrap(t *testing.T) {
	e := gitlab.NewClientNotCreatedError(assert.AnError)
	assert.Equal(t, assert.AnError, e.Unwrap())
}

func TestClientNotCreatedError_Is(t *testing.T) {
	e := gitlab.NewClientNotCreatedError(nil)
	e2 := gitlab.NewClientNotCreatedError(nil)
	assert.True(t, e.Is(e2))
	assert.False(t, e.Is(errors.New("test error")))
}

func TestAPIError_Error(t *testing.T) {
	e := gitlab.NewAPIError("test", assert.AnError)
	assert.Contains(t, e.Error(), "requesting test from API")
	assert.Contains(t, e.Error(), assert.AnError.Error())
}

func TestAPIError_Unwrap(t *testing.T) {
	e := gitlab.NewAPIError("test", assert.AnError)
	assert.Equal(t, assert.AnError, e.Unwrap())
}

func TestAPIError_Is(t *testing.T) {
	e := gitlab.NewAPIError("test", nil)
	e2 := gitlab.NewAPIError("test", nil)
	assert.True(t, e.Is(e2))
	assert.False(t, e.Is(errors.New("test error")))
}

func TestNewClientWithBaseURL(t *testing.T) {
	tests := map[string]struct {
		baseURL       string
		expectedError error
	}{
		"invalid URL": {
			baseURL:       "1://test",
			expectedError: new(gitlab.ClientNotCreatedError),
		},
		"valid URL": {
			baseURL: "https://gitlab.example.com/",
		},
		"empty URL": {},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			c, err := gitlab.NewClientWithBaseURL("fake", "fake", tt.baseURL)

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
			assert.NotNil(t, c)
		})
	}
}

func TestClient_ListMergeRequests(t *testing.T) {
	testToken := "TeStToKeN"
	testProjectID := "1"

	mr1Response := `{"iid":1,"title":"test1","labels":["label 1","label 2"],"author":{"name":"Author Name","username":"author_name"}}`
	mr2Response := `{"iid":2,"title":"test2","labels":["label 1","label 2"],"author":{"name":"Author Name","username":"author_name"}}`
	mrsResponse := fmt.Sprintf("[%s,%s]", mr1Response, mr2Response)
	expectedMRs := []gitlab.MergeRequest{
		{IID: 1, Title: "test1", Labels: []string{"label 1", "label 2"}, AuthorName: "Author Name", AuthorHandle: "author_name"},
		{IID: 2, Title: "test2", Labels: []string{"label 1", "label 2"}, AuthorName: "Author Name", AuthorHandle: "author_name"},
	}

	tests := map[string]struct {
		mrIIDs           []int
		perPage          int
		handlerGenerator func(t *testing.T) (http.HandlerFunc, func())
		expectedError    error
		expectedMRs      []gitlab.MergeRequest
	}{
		"API error": {
			mrIIDs: []int{1, 2, 3, 4},
			handlerGenerator: func(t *testing.T) (http.HandlerFunc, func()) {
				return func(rw http.ResponseWriter, r *http.Request) {}, func() {}
			},
			expectedError: new(gitlab.APIError),
		},
		"no MR IIDs": {
			handlerGenerator: func(t *testing.T) (http.HandlerFunc, func()) {
				handler := func(rw http.ResponseWriter, r *http.Request) {
					requestURI := r.RequestURI

					assert.Contains(t, requestURI, fmt.Sprintf("per_page=%d", gitlab.DefaultPerPage))

					rw.WriteHeader(http.StatusOK)
				}

				return handler, func() {}
			},
			expectedMRs: []gitlab.MergeRequest{},
		},
		"MR IIDs defined": {
			mrIIDs: []int{1, 2, 3, 4},
			handlerGenerator: func(t *testing.T) (http.HandlerFunc, func()) {
				handler := func(rw http.ResponseWriter, r *http.Request) {
					requestURI := r.RequestURI

					assert.Contains(t, requestURI, fmt.Sprintf("per_page=%d", gitlab.DefaultPerPage))

					q := r.URL.Query()
					require.NotEmpty(t, q[`iids[]`])
					assert.Contains(t, q[`iids[]`], "1")
					assert.Contains(t, q[`iids[]`], "2")
					assert.Contains(t, q[`iids[]`], "3")
					assert.Contains(t, q[`iids[]`], "4")

					rw.WriteHeader(http.StatusOK)
					_, err := fmt.Fprintf(rw, mrsResponse)
					require.NoError(t, err)
				}

				return handler, func() {}
			},
			expectedMRs: expectedMRs,
		},
		"prePage defined": {
			perPage: 10,
			mrIIDs:  []int{1, 2, 3, 4},
			handlerGenerator: func(t *testing.T) (http.HandlerFunc, func()) {
				handler := func(rw http.ResponseWriter, r *http.Request) {
					requestURI := r.RequestURI

					assert.Contains(t, requestURI, "per_page=10")

					rw.WriteHeader(http.StatusOK)
					_, err := fmt.Fprintf(rw, mrsResponse)
					require.NoError(t, err)
				}

				return handler, func() {}
			},
			expectedMRs: expectedMRs,
		},
		"pagination triggered": {
			perPage: 1,
			mrIIDs:  []int{1, 2, 3},
			handlerGenerator: func(t *testing.T) (http.HandlerFunc, func()) {
				mr1Requested := false
				mr2Requested := false
				mr3Requested := false

				handler := func(rw http.ResponseWriter, r *http.Request) {
					requestURI := r.RequestURI

					assert.Contains(t, requestURI, "per_page=1")
					assert.Contains(t, requestURI, "page=1")

					q := r.URL.Query()
					require.Contains(t, q, "iids[]")

					var response string

					requestedMR := q["iids[]"][0]
					switch requestedMR {
					case "1":
						mr1Requested = true
						response = fmt.Sprintf("[%s]", mr1Response)
					case "2":
						mr2Requested = true
						response = fmt.Sprintf("[%s]", mr2Response)
					case "3":
						mr3Requested = true
						response = "[]"
					}

					rw.WriteHeader(http.StatusOK)

					_, err := fmt.Fprintf(rw, response)
					require.NoError(t, err)
				}

				assertExpectations := func() {
					assert.True(t, mr1Requested, "MR 1 was not requested")
					assert.True(t, mr2Requested, "MR 2 was not requested")
					assert.True(t, mr3Requested, "MR 3 was not requested")
				}

				return handler, assertExpectations
			},
			expectedMRs: expectedMRs,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			handler, assertExpectations := tt.handlerGenerator(t)
			defer assertExpectations()

			s := httptest.NewServer(handleRateLimitCheckRequest(handler))
			defer s.Close()

			c, err := gitlab.NewClientWithBaseURL(testToken, testProjectID, s.URL)
			require.NoError(t, err)
			require.NotNil(t, c)

			mrs, err := c.ListMergeRequests(tt.mrIIDs, tt.perPage)
			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.expectedMRs, mrs)
		})
	}
}

// The go-gitlab library makes now - at the very beginning - one additional
// request to `/api/v4/` to find out if there are any RateLimit information
// that should be applied. Since it's not important for our tests, we're
// just catching such request and immediately respond with `200 OK`.
func handleRateLimitCheckRequest(inner http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.RequestURI == "/api/v4/" {
			rw.WriteHeader(http.StatusOK)
			return
		}

		if inner != nil {
			inner(rw, r)
		}
	}
}

func TestMergeRequest_Author(t *testing.T) {
	tests := map[string]struct {
		authorName     string
		authorHandle   string
		expectedOutput string
	}{
		"not defined": {
			expectedOutput: "",
		},
		"only handler": {
			authorHandle:   "username",
			expectedOutput: "",
		},
		"only name": {
			authorName:     "User Name",
			expectedOutput: "User Name",
		},
		"both defined": {
			authorName:     "User Name",
			authorHandle:   "username",
			expectedOutput: "User Name @username",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			mr := gitlab.MergeRequest{
				AuthorName:   tt.authorName,
				AuthorHandle: tt.authorHandle,
			}

			assert.Equal(t, tt.expectedOutput, mr.Author())
		})
	}
}
