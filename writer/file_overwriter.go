package writer

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

// NewFileOverwriter creates a new FileOverwriter.
func NewFileOverwriter(filepath string, start []byte, end []byte) io.WriteCloser {
	return &fileOverwriter{
		start:    bytes.NewBuffer(start),
		end:      bytes.NewBuffer(end),
		buffer:   new(bytes.Buffer),
		filepath: filepath,
	}
}

// fileOverwriter is an io.WriteCloser that will overwrite the contents of a file
// with the contents of the buffer accumulated by the Write method.
// The final file will be overwritten by writing the start bytes, the buffer and the end bytes.
type fileOverwriter struct {
	start    *bytes.Buffer
	end      *bytes.Buffer
	buffer   *bytes.Buffer
	filepath string
}

func (w *fileOverwriter) Write(p []byte) (int, error) {
	return w.buffer.Write(p)
}

func (w *fileOverwriter) Close() error {
	return w.overwriteFile()
}

func (w *fileOverwriter) overwriteFile() error {
	overwriteChangelogFile, err := os.OpenFile(w.filepath, os.O_RDWR|os.O_APPEND|os.O_TRUNC, 0600)
	if err != nil {
		return fmt.Errorf("couldn't open new changelog file: %w", err)
	}
	defer overwriteChangelogFile.Close()

	// NOTE: this implementation used to use file descriptors to sync changelog entries
	// from one file to another. Now we are using in-memory buffers which makes the whole
	// thing a lot simpler. The memory footprint is minimal since at the time of writing GitLab Runner
	// has a changelog file with the size of 181k
	buffers := []*bytes.Buffer{
		w.start,
		w.buffer,
		w.end,
	}

	for _, buffer := range buffers {
		if _, err := buffer.WriteTo(overwriteChangelogFile); err != nil {
			return fmt.Errorf("couldn't write new content to the %q file: %w", overwriteChangelogFile.Name(), err)
		}
	}

	return nil
}
