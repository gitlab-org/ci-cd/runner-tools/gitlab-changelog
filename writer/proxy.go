package writer

import (
	"io"
)

func NoopWriteCloser(inner io.WriteCloser) io.WriteCloser {
	return &noopWriteCloser{
		inner: inner,
	}
}

type noopWriteCloser struct {
	inner io.Writer
}

func (w *noopWriteCloser) Write(p []byte) (int, error) {
	return w.inner.Write(p)
}

func (w *noopWriteCloser) Close() error {
	return nil
}
