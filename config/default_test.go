package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
)

func TestDumpDefaultConfig(t *testing.T) {
	data, err := config.DumpDefaultConfig()
	output := string(data)
	assert.NoError(t, err)
	assert.Contains(t, output, "default_scope: other")
}
