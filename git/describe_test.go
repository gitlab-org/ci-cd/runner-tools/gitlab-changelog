package git_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/commander"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"
)

func TestDescribeWithMatchOpts_Args(t *testing.T) {
	tests := map[string]struct {
		opts         *git.DescribeOpts
		expectedArgs []string
	}{
		"empty opts": {
			opts:         new(git.DescribeOpts),
			expectedArgs: []string{"--abbrev=0"},
		},
		"abbrev defined": {
			opts: &git.DescribeOpts{
				Abbrev: 5,
			},
			expectedArgs: []string{"--abbrev=5"},
		},
		"match defined": {
			opts: &git.DescribeOpts{
				Match: "match",
			},
			expectedArgs: []string{"--abbrev=0", "--match", "match"},
		},
		"tags true": {
			opts: &git.DescribeOpts{
				Tags: true,
			},
			expectedArgs: []string{"--abbrev=0", "--tags"},
		},
		"tags false": {
			opts: &git.DescribeOpts{
				Tags: false,
			},
			expectedArgs: []string{"--abbrev=0"},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			args := tt.opts.Args()
			for _, expectedArg := range tt.expectedArgs {
				assert.Contains(t, args, expectedArg)
			}
		})
	}
}

func TestGit_Describe(t *testing.T) {
	testOutput := `
test output
`
	expectedTestOutput := "test output"

	tests := map[string]struct {
		opts             *git.DescribeOpts
		prepareCommander func(*testing.T) (*commander.MockCommander, commander.Factory)
		expectedOutput   string
		expectedError    error
	}{
		"no opts provided": {
			prepareCommander: func(t *testing.T) (*commander.MockCommander, commander.Factory) {
				c := new(commander.MockCommander)
				c.On("Output").
					Return([]byte(testOutput), nil).
					Once()

				factory := func(_ context.Context, command string, args ...string) commander.Commander {
					assert.Equal(t, "git", command)
					require.Len(t, args, 2)
					assert.Equal(t, "describe", args[0])
					assert.Contains(t, args, "--abbrev=7")

					return c
				}

				return c, factory
			},
			expectedOutput: expectedTestOutput,
		},
		"opts provided": {
			opts: &git.DescribeOpts{
				Abbrev: 0,
				Match:  "match",
				Tags:   true,
			},
			prepareCommander: func(t *testing.T) (*commander.MockCommander, commander.Factory) {
				c := new(commander.MockCommander)
				c.On("Output").
					Return([]byte(testOutput), nil).
					Once()

				factory := func(_ context.Context, command string, args ...string) commander.Commander {
					assert.Equal(t, "git", command)
					require.Len(t, args, 5)
					assert.Equal(t, "describe", args[0])
					assert.Contains(t, args, "--abbrev=0")
					assert.Contains(t, args, "--match")
					assert.Contains(t, args, "match")
					assert.Contains(t, args, "--tags")

					return c
				}

				return c, factory
			},
			expectedOutput: expectedTestOutput,
		},
		"error on command execution": {
			prepareCommander: func(t *testing.T) (*commander.MockCommander, commander.Factory) {
				c := new(commander.MockCommander)
				c.On("Output").
					Return([]byte(testOutput), assert.AnError).
					Once()
				c.On("String").
					Return("command here").
					Once()

				factory := func(_ context.Context, command string, args ...string) commander.Commander {
					assert.Equal(t, "git", command)
					require.Len(t, args, 2)
					assert.Equal(t, "describe", args[0])
					assert.Contains(t, args, "--abbrev=7")

					return c
				}

				return c, factory
			},
			expectedError: assert.AnError,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			commanderMock, commanderFactory := tt.prepareCommander(t)
			defer commanderMock.AssertExpectations(t)

			g := git.NewWithCommanderFactory(commanderFactory)
			output, err := g.Describe(tt.opts)

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
			assert.Equal(t, tt.expectedOutput, output)
		})
	}
}
