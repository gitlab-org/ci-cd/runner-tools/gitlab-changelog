package commander_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/commander"
)

func TestCommander_Output(t *testing.T) {
	c := commander.New(context.Background(), "go", "", "version")
	data, err := c.Output()
	assert.NoError(t, err)
	assert.Contains(t, string(data), "go version")
}

func TestCommander_String(t *testing.T) {
	c := commander.New(context.Background(), "go", "", "version")
	assert.Regexp(t, ".*go version", c.String())
}
