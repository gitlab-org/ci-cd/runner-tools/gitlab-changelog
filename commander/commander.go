package commander

import (
	"context"
	"os/exec"
)

type Factory func(ctx context.Context, command string, args ...string) Commander

//go:generate mockery --name=Commander --inpackage
type Commander interface {
	Output() ([]byte, error)
	String() string
}

func New(ctx context.Context, command, wd string, args ...string) Commander {
	cmd := exec.CommandContext(ctx, command, args...)
	cmd.Dir = wd

	return cmd
}
