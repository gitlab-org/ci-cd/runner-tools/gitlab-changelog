package scope_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/scope"
)

type fakeObject struct {
	labels        []string
	content       string
	includeAuthor bool
}

func (o *fakeObject) IncludeAuthor() {
	o.includeAuthor = true
}

func (o *fakeObject) Labels() []string {
	return o.labels
}

func (o *fakeObject) Entry() string {
	if !o.includeAuthor {
		return o.content
	}

	return fmt.Sprintf("%s (author)", o.content)
}

func TestEntriesMapBuilder_Build(t *testing.T) {
	scopeFeature := config.Scope("feature")
	scopeBug := config.Scope("bug")
	scopeSecurityBug := config.Scope("security-bug")
	scopeSecurity := config.Scope("security")
	scopeOther := config.Scope("other")

	labelFeature := "feature"
	labelBug := "bug"
	labelSecurity := "security"
	labelSkip := "skip"

	fakeObjects := func() []scope.Object {
		return []scope.Object{
			&fakeObject{
				labels:  []string{labelFeature},
				content: "only feature",
			},
			&fakeObject{
				labels:  []string{labelFeature, labelSecurity},
				content: "feature and security",
			},
			&fakeObject{
				labels:  []string{labelSecurity, labelBug},
				content: "security and bug",
			},
			&fakeObject{
				labels:  []string{labelBug},
				content: "only bug",
			},
			&fakeObject{
				labels:  []string{labelFeature, labelBug},
				content: "feature and bug",
			},
			&fakeObject{
				labels:  []string{labelFeature, labelSkip},
				content: "skipped entry",
			},
			&fakeObject{
				labels:  []string{"unknown-label"},
				content: "unknown label",
			},
			&fakeObject{
				labels:  []string{},
				content: "no labels",
			},
		}
	}

	simpleConfiguration := config.Configuration{
		DefaultScope: scopeOther,
		Names: config.Names{
			scopeOther: "scope name",
		},
		Order: config.Order{scopeOther},
	}

	tests := map[string]struct {
		config                   config.Configuration
		prepareEntriesMapFactory func() (*scope.MockEntriesMap, scope.EntriesMapFactory)

		expectedError error
	}{
		"error on initialization": {
			config: config.Configuration{},
			prepareEntriesMapFactory: func() (*scope.MockEntriesMap, scope.EntriesMapFactory) {
				m := new(scope.MockEntriesMap)

				f := func(_ config.Order, _ config.Names) (scope.EntriesMap, error) {
					return nil, assert.AnError
				}

				return m, f
			},
			expectedError: assert.AnError,
		},
		"valid configuration": {
			config: config.Configuration{
				DefaultScope: scopeOther,
				Names: config.Names{
					scopeFeature:  "scope name",
					scopeBug:      "scope name",
					scopeSecurity: "scope name",
					scopeOther:    "scope name",
				},
				Order: config.Order{scopeFeature, scopeBug, scopeSecurity, scopeOther},
				LabelMatchers: config.LabelMatchers{
					{
						Labels: config.Labels{labelSecurity, labelBug},
						Scope:  scopeSecurityBug,
					},
					{
						Labels: config.Labels{labelSecurity},
						Scope:  scopeSecurity,
					},
					{
						Labels: config.Labels{labelFeature},
						Scope:  scopeFeature,
					},
					{
						Labels: config.Labels{labelBug},
						Scope:  scopeBug,
					},
				},
				AuthorshipLabels:    config.Labels{labelFeature, labelBug},
				SkipChangelogLabels: config.Labels{labelSkip},
			},
			prepareEntriesMapFactory: func() (*scope.MockEntriesMap, scope.EntriesMapFactory) {
				m := new(scope.MockEntriesMap)
				m.On("Add", scopeFeature, "only feature (author)").Return(nil).Once()
				m.On("Add", scopeSecurity, "feature and security (author)").Return(nil).Once()
				m.On("Add", scopeFeature, "feature and bug (author)").Return(nil).Once()
				m.On("Add", scopeSecurityBug, "security and bug (author)").Return(nil).Once()
				m.On("Add", scopeBug, "only bug (author)").Return(nil).Once()
				m.On("Add", scopeOther, "unknown label").Return(nil).Once()
				m.On("Add", scopeOther, "no labels").Return(nil).Once()

				f := func(_ config.Order, _ config.Names) (scope.EntriesMap, error) {
					return m, nil
				}

				return m, f
			},
		},
		"no label matchers": {
			config: simpleConfiguration,
			prepareEntriesMapFactory: func() (*scope.MockEntriesMap, scope.EntriesMapFactory) {
				m := new(scope.MockEntriesMap)
				m.On("Add", scopeOther, "only feature").Return(nil).Once()
				m.On("Add", scopeOther, "feature and security").Return(nil).Once()
				m.On("Add", scopeOther, "feature and bug").Return(nil).Once()
				m.On("Add", scopeOther, "security and bug").Return(nil).Once()
				m.On("Add", scopeOther, "only bug").Return(nil).Once()
				m.On("Add", scopeOther, "skipped entry").Return(nil).Once()
				m.On("Add", scopeOther, "unknown label").Return(nil).Once()
				m.On("Add", scopeOther, "no labels").Return(nil).Once()

				f := func(_ config.Order, _ config.Names) (scope.EntriesMap, error) {
					return m, nil
				}

				return m, f
			},
		},
		"error on adding": {
			config: simpleConfiguration,
			prepareEntriesMapFactory: func() (*scope.MockEntriesMap, scope.EntriesMapFactory) {
				m := new(scope.MockEntriesMap)
				m.On("Add", scopeOther, "only feature").Return(assert.AnError).Once()

				f := func(_ config.Order, _ config.Names) (scope.EntriesMap, error) {
					return m, nil
				}

				return m, f
			},
			expectedError: assert.AnError,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			entriesMapMock, factory := tt.prepareEntriesMapFactory()
			defer entriesMapMock.AssertExpectations(t)

			entriesBuilder := scope.NewEntriesMapBuilderWithFactory(tt.config, factory)

			_, err := entriesBuilder.Build(fakeObjects())

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			assert.NoError(t, err)
		})
	}
}
