package scope

import (
	"fmt"

	"github.com/juliangruber/go-intersect"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
)

//go:generate mockery --name=Object --inpackage
type Object interface {
	IncludeAuthor()
	Labels() config.Labels
	Entry() string
}

//go:generate mockery --name=EntriesMapBuilder --inpackage
type EntriesMapBuilder interface {
	Build(objects []Object) (EntriesMap, error)
}

func NewEntriesMapBuilder(configuration config.Configuration) EntriesMapBuilder {
	return NewEntriesMapBuilderWithFactory(configuration, NewEntriesMap)
}

func NewEntriesMapBuilderWithFactory(configuration config.Configuration, factory EntriesMapFactory) EntriesMapBuilder {
	b := &defaultEntriesMapBuilder{
		configuration:     configuration,
		entriesMapFactory: factory,
	}

	return b
}

type defaultEntriesMapBuilder struct {
	entriesMap    EntriesMap
	configuration config.Configuration

	entriesMapFactory EntriesMapFactory
}

func (b *defaultEntriesMapBuilder) Build(objects []Object) (EntriesMap, error) {
	err := b.prepareMap()
	if err != nil {
		return nil, fmt.Errorf("preparing entries map struct: %w", err)
	}

	for _, object := range objects {
		err = b.mapObject(object)
		if err != nil {
			return nil, fmt.Errorf("maping object: %w", err)
		}
	}

	return b.entriesMap, nil
}

func (b *defaultEntriesMapBuilder) prepareMap() error {
	entries, err := b.entriesMapFactory(b.configuration.Order, b.configuration.Names)
	if err != nil {
		return err
	}

	b.entriesMap = entries

	return nil
}

func (b *defaultEntriesMapBuilder) mapObject(object Object) error {
	b.mapObjectAuthor(object)
	chosenScope := b.mapObjectScope(object.Labels())

	return b.addObjectToMap(chosenScope, object)
}

func (b *defaultEntriesMapBuilder) mapObjectAuthor(object Object) {
	if len(b.configuration.AuthorshipLabels) < 1 {
		return
	}

	intersection := intersect.Hash(b.configuration.AuthorshipLabels, object.Labels())
	if len(intersection) > 0 {
		object.IncludeAuthor()
	}
}

func (b *defaultEntriesMapBuilder) mapObjectScope(objectLabels []string) config.Scope {
	for _, matcher := range b.configuration.LabelMatchers {
		if isSliceEqual(matcher.Labels, objectLabels) {
			return matcher.Scope
		}
	}

	return b.configuration.DefaultScope
}

func (b *defaultEntriesMapBuilder) addObjectToMap(chosenScope config.Scope, object Object) error {
	if len(b.configuration.SkipChangelogLabels) > 0 {
		intersection := intersect.Hash(b.configuration.SkipChangelogLabels, object.Labels())
		if len(intersection) > 0 {
			return nil
		}
	}

	return b.entriesMap.Add(chosenScope, object.Entry())
}

func isSliceEqual(compareSource []string, target []string) bool {
	intersection := intersect.Hash(compareSource, target)

	return len(intersection) == len(compareSource)
}
