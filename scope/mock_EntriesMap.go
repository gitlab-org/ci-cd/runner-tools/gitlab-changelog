// Code generated by mockery v2.23.1. DO NOT EDIT.

package scope

import (
	mock "github.com/stretchr/testify/mock"
	config "gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
)

// MockEntriesMap is an autogenerated mock type for the EntriesMap type
type MockEntriesMap struct {
	mock.Mock
}

// Add provides a mock function with given fields: scope, entry
func (_m *MockEntriesMap) Add(scope config.Scope, entry string) error {
	ret := _m.Called(scope, entry)

	var r0 error
	if rf, ok := ret.Get(0).(func(config.Scope, string) error); ok {
		r0 = rf(scope, entry)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// AddToFront provides a mock function with given fields: scope, entry
func (_m *MockEntriesMap) AddToFront(scope config.Scope, entry string) error {
	ret := _m.Called(scope, entry)

	var r0 error
	if rf, ok := ret.Get(0).(func(config.Scope, string) error); ok {
		r0 = rf(scope, entry)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ForEach provides a mock function with given fields: fn
func (_m *MockEntriesMap) ForEach(fn func(Entries) error) error {
	ret := _m.Called(fn)

	var r0 error
	if rf, ok := ret.Get(0).(func(func(Entries) error) error); ok {
		r0 = rf(fn)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Len provides a mock function with given fields:
func (_m *MockEntriesMap) Len() int {
	ret := _m.Called()

	var r0 int
	if rf, ok := ret.Get(0).(func() int); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int)
	}

	return r0
}

type mockConstructorTestingTNewMockEntriesMap interface {
	mock.TestingT
	Cleanup(func())
}

// NewMockEntriesMap creates a new instance of MockEntriesMap. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewMockEntriesMap(t mockConstructorTestingTNewMockEntriesMap) *MockEntriesMap {
	mock := &MockEntriesMap{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
