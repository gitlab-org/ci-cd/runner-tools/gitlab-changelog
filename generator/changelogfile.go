package generator

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/writer"
)

type changelogFile struct {
	// inner is the underlying FilePrepender as an io.WriteCloser
	inner io.WriteCloser

	// mrs and release are the name of the last release
	// and the amount of mrs in it
	mrs     int
	release string
}

func newChangelogFile(opts Opts) (*changelogFile, error) {
	if opts.ChangelogFile == "" {
		return &changelogFile{
			inner: writer.NoopWriteCloser(os.Stdout),
		}, nil
	}

	logrus.WithField("file", opts.ChangelogFile).
		Info("Writing output to file")

	return readChangelogFile(opts)
}

// readChangelogFile returns an *os.File that will be used to write the changelog entries to
// the callers of this function are responsible for making sure the file is closed if no error is returned
func readChangelogFile(opts Opts) (*changelogFile, error) {
	logrus.WithField("file", opts.ChangelogFile).
		Info("Reading changelog file")

	f, err := os.OpenFile(opts.ChangelogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0600)
	if err != nil {
		return nil, fmt.Errorf("opening changelog file: %w", err)
	}
	defer f.Close()

	meta, err := findReleaseMetadataFromChangelog(f, opts)
	if err != nil {
		return nil, fmt.Errorf("finding release version from changelog: %w", err)
	}

	if !opts.OverwriteRelease {
		cf := &changelogFile{
			inner: writer.NewFileOverwriter(f.Name(), nil, meta.buf.Bytes()),
		}

		return cf, nil
	}

	cf := &changelogFile{
		mrs:     meta.mrsCount,
		release: meta.version,
	}

	// if we couldn't find a release to overwrite seek back to the start of the file and return it as is
	if cf.release == "" {
		cf.inner = writer.NewFileOverwriter(f.Name(), nil, meta.buf.Bytes())
		return cf, nil
	}

	startBuf := meta.buf.Bytes()[:meta.indexStart]
	endBuf := meta.buf.Bytes()[meta.indexEnd:]

	cf.inner = writer.NewFileOverwriter(f.Name(), startBuf, endBuf)

	return cf, nil
}

type releaseMetadata struct {
	buf        *bytes.Buffer
	indexEnd   int
	indexStart int
	mrsCount   int
	version    string
}

// findReleaseMetadataFromChangelog finds the version of the last release in a changelog file
// returns the index in the file where the previous release starts and the version of the current release
//
//gocyclo:ignore
func findReleaseMetadataFromChangelog(f io.Reader, opts Opts) (releaseMetadata, error) {
	b, err := io.ReadAll(f)
	if err != nil {
		return releaseMetadata{}, fmt.Errorf("reading changelog file: %w", err)
	}

	buf := bytes.NewBuffer(b)
	scanner := bufio.NewScanner(bytes.NewReader(buf.Bytes()[:]))

	splitter := &lineSplitter{}
	scanner.Split(splitter.Split)

	var indexStart int
	var indexEnd int
	var foundVersion string
	var mrsCount int

	for scanner.Scan() {
		line := scanner.Text()
		if foundVersion == opts.Release && strings.HasPrefix(line, "- ") {
			mrsCount++
		}

		// first hit is the current release
		if strings.HasPrefix(line, "## ") {
			// second hit is the previous release
			// if there's only one release in the changelog we'll naturally reach the end
			// and the whole file will be overwritten
			if foundVersion != "" {
				indexEnd = splitter.index - len(line) - 1
				break
			}

			// figure out the current release version
			v := versionRegex.FindString(line)
			if v == "" {
				return releaseMetadata{}, fmt.Errorf("cannot extract release version from changelog line %s", line)
			}

			if v == opts.Release {
				foundVersion = v
				// we need to keep track of the index, so we can cut out the current release from the changelog
				// in case it's the same version as the one we're generating, so it can be overwritten
				// make sure to not update the index when we find the previous release
				indexStart = splitter.index - len(line) - 1
			}
		}
	}

	// reach the end of the file in all cases for proper splitter.index calculation
	for scanner.Scan() {
	}

	if indexEnd == 0 {
		indexEnd = splitter.index
	}

	return releaseMetadata{
		buf:        buf,
		indexStart: indexStart,
		indexEnd:   indexEnd,
		mrsCount:   mrsCount,
		version:    foundVersion,
	}, nil
}
