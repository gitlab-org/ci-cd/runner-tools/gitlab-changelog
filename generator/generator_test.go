package generator

import (
	"fmt"
	"io"
	"os"
	"testing"
	"time"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/extractor"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/gitlab"

	"github.com/stretchr/testify/require"
)

const changelog = `## v0.6.0 (2020-11-13)

### Other changes

- test [!1](https://gitlab.example.com/1)

## v0.5.0 (2020-09-11)

### Maintenance

- maintenance [!2](https://gitlab.example.com/2)
`

func newRelease(release string) string {
	return fmt.Sprintf(`## %s (%s)

### Other changes

- test [!1](https://gitlab.example.com/1)

`, release, time.Now().Format(RFC3339Date))
}

func prependChangelogReleaseFromToday(release string) string {
	return fmt.Sprintf(`## %s (%s)

### Other changes

- test [!1](https://gitlab.example.com/1)

%s`, release, time.Now().Format(RFC3339Date), changelog)
}

func prependChangelogReleaseFromTodayEmpty(release string) string {
	return fmt.Sprintf(`## %s (%s)

%s`, release, time.Now().Format(RFC3339Date), changelog)
}

func generateTempChangelogFile(t *testing.T) string {
	f, err := os.CreateTemp("", "changelog")
	require.NoError(t, err)

	_, err = f.WriteString(changelog)
	require.NoError(t, err)

	require.NoError(t, f.Close())

	return f.Name()
}

func TestGenerate(t *testing.T) {
	tests := map[string]struct {
		opts                  Opts
		generateChangelogFile func(t *testing.T) string
		assertFn              func(t *testing.T, newChangelog string)
	}{
		"dont overwrite release": {
			opts: Opts{
				OverwriteRelease: false,
				Release:          "v0.6.0",
				StartingPoint:    "v0.5.0",
				MrIIDs:           []int{1},
			},
			generateChangelogFile: generateTempChangelogFile,
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, prependChangelogReleaseFromToday("v0.6.0"), newChangelog)
			},
		},
		"don't overwrite release with the same amount of mrs": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.6.0",
				StartingPoint:    "v0.5.0",
				MrIIDs:           []int{1},
			},
			generateChangelogFile: generateTempChangelogFile,
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, changelog, newChangelog)
			},
		},
		"overwrite last release with a new MR": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.6.0",
				StartingPoint:    "v0.5.0",
				MrIIDs:           []int{1, 2},
			},
			generateChangelogFile: generateTempChangelogFile,
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, fmt.Sprintf(`## v0.6.0 (%s)

### Other changes

- test [!1](https://gitlab.example.com/1)
- test [!2](https://gitlab.example.com/2)

## v0.5.0 (2020-09-11)

### Maintenance

- maintenance [!2](https://gitlab.example.com/2)
`, time.Now().Format(RFC3339Date)), newChangelog)
			},
		},
		"generate new version": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.7.0",
				StartingPoint:    "v0.6.0",
				MrIIDs:           []int{1},
			},
			generateChangelogFile: generateTempChangelogFile,
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, prependChangelogReleaseFromToday("v0.7.0"), newChangelog)
			},
		},
		"empty changelog": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.7.0",
				StartingPoint:    "v0.6.0",
				MrIIDs:           []int{1},
			},
			generateChangelogFile: func(t *testing.T) string {
				f, err := os.CreateTemp("", "changelog")
				require.NoError(t, err)
				require.NoError(t, f.Close())

				return f.Name()
			},
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, newRelease("v0.7.0"), newChangelog)
			},
		},
		"only one release in changelog overwrite": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.6.0",
				StartingPoint:    "v0.5.0",
				MrIIDs:           []int{1},
			},
			generateChangelogFile: func(t *testing.T) string {
				f, err := os.CreateTemp("", "changelog")
				require.NoError(t, err)

				_, err = f.WriteString(`## v0.6.0 (2020-11-13)

		### Bug fixes

		- bug fix [!2](https://gitlab.example.com/2)

		`)
				require.NoError(t, err)
				require.NoError(t, f.Close())
				return f.Name()
			},
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, newRelease("v0.6.0"), newChangelog)
			},
		},
		"changelog file path doesn't exist": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.6.0",
				StartingPoint:    "v0.5.0",
				MrIIDs:           []int{1},
			},
			generateChangelogFile: func(t *testing.T) string {
				f, err := os.CreateTemp("", "changelog")
				require.NoError(t, err)

				require.NoError(t, f.Close())
				require.NoError(t, os.Remove(f.Name()))

				return f.Name()
			},
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, newRelease("v0.6.0"), newChangelog)
			},
		},
		"generate new release no mrs": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.7.0",
				StartingPoint:    "v0.6.0",
			},
			generateChangelogFile: generateTempChangelogFile,
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, prependChangelogReleaseFromTodayEmpty("v0.7.0"), newChangelog)
			},
		},
		"generate changelog for a previous release": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.5.0",
				StartingPoint:    "v0.6.0",
				MrIIDs:           []int{1, 2},
			},
			generateChangelogFile: generateTempChangelogFile,
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, fmt.Sprintf(`## v0.6.0 (2020-11-13)

### Other changes

- test [!1](https://gitlab.example.com/1)

## v0.5.0 (%s)

### Other changes

- test [!1](https://gitlab.example.com/1)
- test [!2](https://gitlab.example.com/2)

`, time.Now().Format(RFC3339Date)), newChangelog)
			},
		},
		"generate changelog for a previous release by provided MRs while MrIIDs are ignored": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.5.0",
				StartingPoint:    "v0.6.0",
				MrIIDs:           []int{3, 4},
				MRs: []gitlab.MergeRequest{
					{
						IID:   1,
						Title: "test",
						URL:   "https://gitlab.example.com/1",
					},
					{
						IID:   2,
						Title: "test",
						URL:   "https://gitlab.example.com/2",
					},
				},
			},
			generateChangelogFile: generateTempChangelogFile,
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, fmt.Sprintf(`## v0.6.0 (2020-11-13)

### Other changes

- test [!1](https://gitlab.example.com/1)

## v0.5.0 (%s)

### Other changes

- test [!1](https://gitlab.example.com/1)
- test [!2](https://gitlab.example.com/2)

`, time.Now().Format(RFC3339Date)), newChangelog)
			},
		},
		"generate changelog for a previous release in the middle": {
			opts: Opts{
				OverwriteRelease: true,
				Release:          "v0.5.0",
				StartingPoint:    "v0.6.0",
				MrIIDs:           []int{2, 3},
			},
			generateChangelogFile: func(t *testing.T) string {
				f, err := os.CreateTemp("", "changelog")
				require.NoError(t, err)

				const changelog = `## v0.6.0 (2020-11-13)

### Other changes

- test [!1](https://gitlab.example.com/1)

## v0.5.0 (2020-09-11)

### Maintenance

- maintenance [!1](https://gitlab.example.com/1)

## v0.4.0 (2020-11-13)

### Other changes

- test [!4](https://gitlab.example.com/4)

`

				_, err = f.WriteString(changelog)
				require.NoError(t, err)

				return f.Name()
			},
			assertFn: func(t *testing.T, newChangelog string) {
				require.Equal(t, fmt.Sprintf(`## v0.6.0 (2020-11-13)

### Other changes

- test [!1](https://gitlab.example.com/1)

## v0.5.0 (%s)

### Other changes

- test [!2](https://gitlab.example.com/2)
- test [!3](https://gitlab.example.com/3)

## v0.4.0 (2020-11-13)

### Other changes

- test [!4](https://gitlab.example.com/4)

`, time.Now().Format(RFC3339Date)), newChangelog)
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			tt.opts.ChangelogFile = tt.generateChangelogFile(t)
			generator, err := New(tt.opts)
			require.NoError(t, err)

			mockExtractor := &extractor.MockExtractor{}
			mockExtractor.On("ExtractMRIIDs", tt.opts.StartingPoint, "").
				Maybe().
				Return([]int{}, nil)

			defer mockExtractor.AssertExpectations(t)

			mockClient := &gitlab.MockClient{}
			if len(tt.opts.MrIIDs) > 0 && len(tt.opts.MRs) == 0 {
				var results []gitlab.MergeRequest
				for _, mr := range tt.opts.MrIIDs {
					results = append(results, gitlab.MergeRequest{
						IID:   mr,
						Title: "test",
						URL:   "https://gitlab.example.com/" + fmt.Sprint(mr),
					})
				}

				mockClient.On("ListMergeRequests", tt.opts.MrIIDs, len(tt.opts.MrIIDs)).
					Return(results, nil)
				defer mockClient.AssertExpectations(t)
			}

			generator.gitlabClient = mockClient
			generator.extractor = mockExtractor

			require.NoError(t, generator.Generate())

			f, err := os.Open(tt.opts.ChangelogFile)
			require.NoError(t, err)

			b, err := io.ReadAll(f)
			require.NoError(t, err)

			tt.assertFn(t, string(b))
		})
	}
}
