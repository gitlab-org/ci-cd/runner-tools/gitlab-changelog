package generator

import (
	"fmt"
	"io"
	"os"
	"regexp"
	"time"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/config"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/extractor"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/scope"
)

const (
	RFC3339Date = "2006-01-02"

	privateTokenEnv = "GITLAB_PRIVATE_TOKEN"
)

var versionRegex = regexp.MustCompile(`v\d+\.\d+\.\d+`)

type Opts struct {
	ProjectID string
	Release   string

	StartingPoint        string
	StartingPointMatcher string

	ChangelogFile    string
	ConfigFile       string
	WorkingDirectory string
	OverwriteRelease bool

	// MrIIDs is used to provide a list of MrIIDs in case the IIDs are known but not the MRs themselves are not
	MrIIDs []int
	// MRs is used to provide a list of MRs to be added to the changelog. No API requests will be made. MRs takes precedence over MrIIDs
	MRs               []gitlab.MergeRequest
	AdditionalEntries []OptsAdditionalEntry
}

type OptsAdditionalEntry struct {
	Scope config.Scope
	Entry string
}

type Generator struct {
	opts        Opts
	scopeConfig config.Configuration

	extractor    extractor.Extractor
	gitlabClient gitlab.Client
}

func New(opts Opts) (*Generator, error) {
	generator := &Generator{opts: opts}

	if err := generator.setupConfiguration(); err != nil {
		return nil, fmt.Errorf("loading scope configuration file: %w", err)
	}

	generator.extractor = extractor.New(git.New(generator.opts.WorkingDirectory))

	var err error
	generator.gitlabClient, err = gitlab.NewClient(os.Getenv(privateTokenEnv), opts.ProjectID)
	if err != nil {
		return nil, fmt.Errorf("couldn't initialize GitLab client: %w", err)
	}

	return generator, nil
}

func (g *Generator) Generate() error {
	logrus.WithField("release", g.opts.Release).
		Info("Generating changelog entries")

	entries, err := g.buildEntries()
	if err != nil {
		return fmt.Errorf("building changelog entries: %w", err)
	}

	cf, err := newChangelogFile(g.opts)
	if err != nil {
		return fmt.Errorf("getting changelog file: %w", err)
	}

	// TODO: this detection isn't perfect. If we take out one entry but add a different one
	// this will still be true. We should probably check the actual entries.
	if cf.release == g.opts.Release && cf.mrs == entries.Len() {
		logrus.WithField("release", g.opts.Release).
			Info("Changelog entries already generated")
		return nil
	}

	err = g.writeEntries(cf.inner, entries)
	if err != nil {
		return fmt.Errorf("writing changelog entries: %w", err)
	}

	logrus.WithField("release", g.opts.Release).
		Info("Changelog entries generated")

	return nil
}

func (g *Generator) buildEntries() (scope.EntriesMap, error) {
	mrIIDs, err := g.getMRIIDs()
	if err != nil {
		return nil, fmt.Errorf("extracting MR IIDs from git history: %w", err)
	}

	mergeRequests, err := g.getMergeRequests(mrIIDs)
	if err != nil {
		return nil, fmt.Errorf("generating merge requests list: %w", err)
	}

	mergeRequestObjects := g.convertMergeRequestsToScopeObjets(mergeRequests)
	entries, err := g.buildEntriesMap(mergeRequestObjects)
	if err != nil {
		return nil, fmt.Errorf("building entries map: %w", err)
	}

	for _, entry := range g.opts.AdditionalEntries {
		if err := entries.AddToFront(entry.Scope, entry.Entry); err != nil {
			return nil, fmt.Errorf("adding additional entry: %w", err)
		}
	}

	return entries, nil
}

func (g *Generator) setupConfiguration() error {
	if g.opts.ConfigFile == "" {
		g.scopeConfig = config.DefaultConfig()
		return nil
	}

	cnf, err := config.LoadConfig(g.opts.ConfigFile)
	if err != nil {
		return err
	}

	g.scopeConfig = cnf

	return nil
}

func (g *Generator) getMRIIDs() ([]int, error) {
	if len(g.opts.MrIIDs) > 0 {
		logrus.WithField("count", len(g.opts.MrIIDs)).
			Info("Using config provided merge request IIDs")

		return g.opts.MrIIDs, nil
	} else if len(g.opts.MRs) > 0 {
		return nil, nil
	}

	mrIIDs, err := g.extractor.ExtractMRIIDs(g.opts.StartingPoint, g.opts.StartingPointMatcher)
	if err != nil {
		return nil, fmt.Errorf("extracting merge request IIDs: %w", err)
	}

	logrus.WithField("count", len(mrIIDs)).
		Info("Found merge requests commits")

	return mrIIDs, nil
}

func (g *Generator) getMergeRequests(mrIIDs []int) ([]gitlab.MergeRequest, error) {
	if len(g.opts.MRs) > 0 {
		logrus.WithField("count", len(g.opts.MRs)).
			Info("Found provided merge requests")

		return g.opts.MRs, nil
	}

	if len(mrIIDs) < 1 {
		return []gitlab.MergeRequest{}, nil
	}

	mergeRequests, err := g.gitlabClient.ListMergeRequests(mrIIDs, len(mrIIDs))
	if err != nil {
		return nil, fmt.Errorf("listing merge requests: %w", err)
	}

	logrus.WithField("count", len(mergeRequests)).
		Info("Found merge requests")

	return mergeRequests, nil
}

func (g *Generator) convertMergeRequestsToScopeObjets(mergeRequests []gitlab.MergeRequest) []scope.Object {
	objects := make([]scope.Object, 0)
	for _, mr := range mergeRequests {
		objects = append(objects, NewMRScopeObjectAdapter(mr))
	}

	return objects
}

func (g *Generator) buildEntriesMap(objects []scope.Object) (scope.EntriesMap, error) {
	entriesBuilder := scope.NewEntriesMapBuilder(g.scopeConfig)
	entries, err := entriesBuilder.Build(objects)
	if err != nil {
		return nil, err
	}

	return entries, nil
}

func (g *Generator) writeEntries(w io.WriteCloser, entries scope.EntriesMap) error {
	_, err := fmt.Fprintf(w, "## %s (%s)\n\n", g.opts.Release, time.Now().Format(RFC3339Date))
	if err != nil {
		return err
	}

	err = entries.ForEach(func(entry scope.Entries) error {
		_, err = fmt.Fprintf(w, "### %s\n\n", entry.ScopeName)
		if err != nil {
			return err
		}
		for _, scopeEntry := range entry.Entries {
			_, err = fmt.Fprintf(w, "- %s\n", scopeEntry)
			if err != nil {
				return err
			}
		}
		_, err = fmt.Fprintln(w)
		return err
	})

	if err != nil {
		return err
	}

	return w.Close()
}
