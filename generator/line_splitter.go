package generator

import "bufio"

// lineSplitter is a struct with a Split function that's a bufio.SplitFunc
// it also keeps track of the index it has reached in the file
type lineSplitter struct {
	index int
}

func (s *lineSplitter) Split(data []byte, atEOF bool) (advance int, token []byte, err error) {
	advance, token, err = bufio.ScanLines(data, atEOF)
	s.index += advance

	return advance, token, err
}
