package extractor

import (
	"bufio"
	"bytes"
	"fmt"
	"regexp"
	"strconv"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"
)

var (
	mrRegexFullURL     = regexp.MustCompile(`^\s*See merge request .+merge_requests/([0-9]+)$`)
	mrRegexExclamation = regexp.MustCompile(`^\s*See merge request (https?://)?([a-zA-Z0-9-_/.]+)?!([0-9]+)$`)
)

//go:generate mockery --name=Extractor --inpackage
type Extractor interface {
	ExtractMRIIDs(startingPoint string, matcher string) ([]int, error)
	GetStartingPoint(startingPoint string, matcher string) (string, error)
}

type defaultExtractor struct {
	gitReader git.Git
}

func New(gitReader git.Git) Extractor {
	return &defaultExtractor{
		gitReader: gitReader,
	}
}

func (e *defaultExtractor) ExtractMRIIDs(startingPoint string, matcher string) ([]int, error) {
	sp, err := e.GetStartingPoint(startingPoint, matcher)
	if err != nil {
		return nil, fmt.Errorf("establihing starting point: %w", err)
	}

	logOutput, err := e.getLogOutput(sp)
	if err != nil {
		return nil, fmt.Errorf("reading git log output: %w", err)
	}

	mrIIDs := make(map[int]struct{})

	reader := bytes.NewReader(logOutput)
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		mrID := e.extractMergeRequestID(scanner.Text())
		if mrID == "" {
			continue
		}

		id, err := strconv.Atoi(mrID)
		if err != nil {
			return nil, fmt.Errorf("couldn't parse ID %q: %w", mrID, err)
		}

		mrIIDs[id] = struct{}{}
	}

	var ids []int
	for id := range mrIIDs {
		ids = append(ids, id)
	}

	return ids, nil
}

func (e *defaultExtractor) extractMergeRequestID(line string) string {
	regexes := []*regexp.Regexp{
		mrRegexExclamation,
		mrRegexFullURL,
	}

	for _, pattern := range regexes {
		matches := pattern.FindStringSubmatch(line)
		if len(matches) == 0 {
			continue
		}

		mrID := matches[len(matches)-1]
		if mrID != "" {
			return mrID
		}
	}

	return ""
}

func (e *defaultExtractor) GetStartingPoint(startingPoint string, matcher string) (string, error) {
	if startingPoint != "" {
		logrus.WithField("starting-point", startingPoint).
			Info("Using provided starting point")

		return startingPoint, nil
	}

	logrus.WithField("starting-point-matcher", matcher).
		Debug("Autodiscovering starting point")

	startingPoint, err := e.gitReader.Describe(&git.DescribeOpts{
		Abbrev: 0,
		Match:  matcher,
		Tags:   true,
	})

	if err != nil {
		return "", err
	}

	logrus.WithField("starting-point", startingPoint).
		Info("Starting point autodiscovered")

	return startingPoint, nil
}

func (e *defaultExtractor) getLogOutput(sp string) ([]byte, error) {
	logOutput, err := e.gitReader.Log(fmt.Sprintf("%s..", sp), &git.LogOpts{})

	if err != nil {
		return nil, err
	}

	return logOutput, nil
}
