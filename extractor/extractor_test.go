package extractor_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/assertions"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/extractor"
	"gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog/git"
)

func TestExtractor_ExtractMRIIDs(t *testing.T) {
	testLogOutput := []byte("test log output")
	testLogOutputWithMRIIDs := []byte(`
test

See merge request !1

test

See merge request !3

test

See merge request !2

test

See merge request https://gitlab.example.com/merge_requests/4

test

See merge request https://gitlab.com/gitlab-org/gitlab-runner!5

test

See merge request gitlab.example.com/merge_requests!6

test

See merge request gitlab.com/gitlab-org/gitlab-runner!7

test

See merge request gitlab-runner!8
`)

	tests := map[string]struct {
		startingPoint  string
		matcher        string
		assertGitMock  func(t *testing.T, g *git.MockGit)
		expectedError  error
		expectedMRIIDs []int
	}{
		"error on listing logs": {
			startingPoint: "starting-point",
			assertGitMock: func(t *testing.T, g *git.MockGit) {
				g.On("Log", "starting-point..", &git.LogOpts{}).
					Return(nil, assert.AnError).
					Once()
			},
			expectedError: assert.AnError,
		},
		"starting point defined directly": {
			startingPoint: "starting-point",
			assertGitMock: func(t *testing.T, g *git.MockGit) {
				g.On("Log", "starting-point..", &git.LogOpts{}).
					Return(testLogOutput, nil).
					Once()
			},
			expectedMRIIDs: []int{},
		},
		"error on starting point discovery": {
			assertGitMock: func(t *testing.T, g *git.MockGit) {
				g.On("Describe", &git.DescribeOpts{
					Abbrev: 0,
					Match:  "",
					Tags:   true,
				}).Return("", assert.AnError).
					Once()
			},
			expectedError: assert.AnError,
		},
		"starting point discovery with custom matcher": {
			matcher: "matcher",
			assertGitMock: func(t *testing.T, g *git.MockGit) {
				g.On("Describe", &git.DescribeOpts{
					Abbrev: 0,
					Match:  "matcher",
					Tags:   true,
				}).Return("test-sp", nil).
					Once()
				g.On("Log", "test-sp..", &git.LogOpts{}).
					Return(testLogOutput, nil).
					Once()
			},
			expectedMRIIDs: []int{},
		},
		"MRIIDs found": {
			startingPoint: "starting-point",
			assertGitMock: func(t *testing.T, g *git.MockGit) {
				g.On("Log", "starting-point..", &git.LogOpts{}).
					Return(testLogOutputWithMRIIDs, nil).
					Once()
			},
			expectedMRIIDs: []int{1, 3, 2, 4, 5, 6, 7, 8},
		},
		"Dedup MRIIDs": {
			startingPoint: "starting-point",
			assertGitMock: func(t *testing.T, g *git.MockGit) {
				g.On("Log", "starting-point..", &git.LogOpts{}).
					Return([]byte(`
test

See merge request !1

test

See merge request !1
`), nil).Once()
			},
			expectedMRIIDs: []int{1},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			gitMock := new(git.MockGit)
			defer gitMock.AssertExpectations(t)

			tt.assertGitMock(t, gitMock)

			e := extractor.New(gitMock)
			mrIIDs, err := e.ExtractMRIIDs(tt.startingPoint, tt.matcher)

			if tt.expectedError != nil {
				assertions.ErrorIs(t, err, tt.expectedError)
				return
			}

			found := map[int]struct{}{}
			for _, id := range mrIIDs {
				found[id] = struct{}{}
			}

			assert.NoError(t, err)
			for _, expected := range tt.expectedMRIIDs {
				assert.Contains(t, found, expected)
			}
		})
	}
}
